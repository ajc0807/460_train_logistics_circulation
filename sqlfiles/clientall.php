<!--
This file is used to display the records from database
Copy this file in C://xampp/htdocs/ and open a browser and run http://localhost/editrecord.php
Before that you should turn on MySQL database server as well as Apache web server.
-->
<?php

$servername = "localhost";// sql server name
$username = "root";// sql username
$password = "";// sql password
$dbname  = "tlc";// database name

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
$sql = "SELECT * FROM client";// embed a select statement in php
$result = $conn->query($sql);// get result
if (!empty($_GET['CID'])){
    $cid = $_GET['CID'];// get the id value from url parameters
}

if(isset($_GET['mode']) == 'delete'){

    $delete_bind=$conn->prepare("DELETE FROM client WHERE CID= ? ");//delete statement
    $delete_bind->bind_param("i", $cid);
    $delete_bind->execute();
    $delete_bind->close();

    if($delete_bind)
 { 
  echo "Record deleted successfully!";
 }
}
echo '<form>
  <a href="mainmenu.php"> Return to Metatable</a>
</form>';
if($result->num_rows > 0){// check for number of rows. If there are records, build a table to show them
 echo "<table style='border: solid 1px black;'>
	<tr style='border: solid 1px black;'>
	    <th style='border: solid 1px black;'>CID</th>
	    <th style='border: solid 1px black;'>Name</th>
	    <th style='border: solid 1px black;'>BirthYear</th>
	    <th style='border: solid 1px black;'>Delete</th>
	    <th style='border: solid 1px black;'>Edit</th>
	</tr>";
}

while ($row = $result -> fetch_assoc()){// Fetch the query result and store them in an array
	echo '<tr style="border: solid 1px black;">
		<td style="border: solid 1px black;">'.$row['CID'].'</td>
		<td style="border: solid 1px black;">'.$row['Name'].'</td>
		<td style="border: solid 1px black;">'.$row['BirthYear'].'</td>

<!-- below, creates a hyperlink (Delete) and change the mode to "delete". Please note that the link is redirected to the same page (href="delete.php"). -->
        <td> <a href="clientall.php?CID='.$row['CID'].'&mode=delete">Delete </a></td>

<!-- the core edit operation is done in edit.php. Here, we create only a hyperlink and send parameters to edit.php -->		
<!--For each row of the table, we create a hyperlink and include the parameter CID to be used it in the destination page (edit.php)-->
		<td style="border: solid 1px black;"> <a href="clientupdate.php?CID='.$row['CID'].'&mode=update">Update </a></td>

		</tr>';
}
 
echo "</table>";
    echo '<form>
    <a href="clientinsert.php"> Insert Element</a>
    </form>';
?>
