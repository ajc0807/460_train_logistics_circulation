<!--
This file is used to edit the records in table persons. You do not need to run this by yourself. 
This is called by the editrecord.php.
-->
<?php
//if (!empty($_GET['TID'])){
//$TID = $_GET['TID']; //the value of pid is received from the editrecord.php page
//}

$servername = "localhost";
$username = "root";
$password = "";
$dbname  = "tlc";

// Create connection to database
$conn = new mysqli($servername, $username, $password, $dbname);

//Things to do, after the "submitbtn" button is clicked.
if(isset($_POST['submitbtn']))
{
        $insert_bind=$conn->prepare("INSERT INTO train (TID, TrainLocation, Tcondition, TimeInUseHours, NextMaintenanceCheck, WeightCapacityLbs) VALUES (DEFAULT, ?, ?, ?, ?, ?)");
        $insert_bind->bind_param("isiii", $_POST['TrainLocationtb'], $_POST['Tconditiontb'], $_POST['TimeInUseHourstb'], $_POST['NextMaintenanceChecktb'], $_POST['WeightCapacityLbstb']);
        $insert_bind->execute();
        $insert_bind->close();

        if($insert_bind) //if the update is done successfully
		{
		echo "Records inserted successfully";
		}
            echo '<form>
                        <a href="trainall.php"> Return to table</a>
                </form>';
}
?>

<form action="" method="post">
<?php
 echo "<table style='border: solid 1px black;'>
	<tr>
	    <th>TID</th>
	    <th>TrainLocation </th>
	    <th>Tcondition</th>
	    <th>TimeInUseHours</th>
	    <th>NextMaintenanceCheck</th>
	    <th>WeightCapacityLbs</th>
	</tr>";

	echo '<tr>
		<!-- just for simplicity, we assume the PK value cannot be updated, as such, it is "readonly" -->
		<td><input type="text" name="TIDtb" " readonly/></td>
		<td><input type="text" name="TrainLocationtb" "/></td>
		<td><input type="text" name="Tconditiontb" "/></td>
		<td><input type="text" name="TimeInUseHourstb" "/></td>
		<td><input type="text" name="NextMaintenanceChecktb" "/></td>
		<td><input type="text" name="WeightCapacityLbstb" "/></td>
	      <tr>';
 echo "</table>";
?>
<input type="submit" value="Submit" name="submitbtn"/>
</form>

