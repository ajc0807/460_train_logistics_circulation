<!--
This file is used to display the records from database
Copy this file in C://xampp/htdocs/ and open a browser and run http://localhost/editrecord.php
Before that you should turn on MySQL database server as well as Apache web server.
-->
<?php

$servername = "localhost";// sql server name
$username = "root";// sql username
$password = "";// sql password
$dbname  = "tlc";// database name

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
$sql = "SELECT * FROM train";// embed a select statement in php
$result = $conn->query($sql);// get result
if (!empty($_GET['TID'])){
    $tid = $_GET['TID'];// get the id value from url parameters
}

if(isset($_GET['mode']) == 'delete'){

    $delete_bind=$conn->prepare("DELETE FROM train WHERE TID= ?");//delete statement
    $delete_bind->bind_param("i", $tid);
    $delete_bind->execute();
    $delete_bind->close();

    if($delete_bind)
 { 
  echo "Record deleted successfully!";
 }
}
echo '<form>
  <a href="mainmenu.php"> Return to Metatable</a>
</form>';
if($result->num_rows > 0){// check for number of rows. If there are records, build a table to show them
 echo "<table style='border: solid 1px black;'>
	<tr style='border: solid 1px black;'>
	    <th style='border: solid 1px black;'>TID</th>
	    <th style='border: solid 1px black;'>TrainLocation</th>
	    <th style='border: solid 1px black;'>Tcondition</th>
	    <th style='border: solid 1px black;'>TimeInUseHours</th>
	    <th style='border: solid 1px black;'>NextMaintenanceCheck</th>
	    <th style='border: solid 1px black;'>WeightCapacityLbs</th>
	    <th style='border: solid 1px black;'>Delete</th>
	    <th style='border: solid 1px black;'>Edit</th>
	</tr>";
}

while ($row = $result -> fetch_assoc()){// Fetch the query result and store them in an array
	echo '<tr style="border: solid 1px black;">
		<td style="border: solid 1px black;">'.$row['TID'].'</td>
		<td style="border: solid 1px black;">'.$row['TrainLocation'].'</td>
		<td style="border: solid 1px black;">'.$row['Tcondition'].'</td>
		<td style="border: solid 1px black;">'.$row['TimeInUseHours'].'</td>
		<td style="border: solid 1px black;">'.$row['NextMaintenanceCheck'].'</td>
		<td style="border: solid 1px black;">'.$row['WeightCapacityLbs'].'</td>

<!-- below, creates a hyperlink (Delete) and change the mode to "delete". Please note that the link is redirected to the same page (href="delete.php"). -->
        <td> <a href="trainall.php?TID='.$row['TID'].'&mode=delete">Delete </a></td>

<!-- the core edit operation is done in edit.php. Here, we create only a hyperlink and send parameters to edit.php -->		
<!--For each row of the table, we create a hyperlink and include the parameter TID to be used it in the destination page (edit.php)-->
		<td style="border: solid 1px black;"> <a href="trainupdate.php?TID='.$row['TID'].'&mode=update">Update </a></td>

		</tr>';
}
 
echo "</table>";
    echo '<form>
    <a href="traininsert.php"> Insert Element</a>
    </form>';
?>
