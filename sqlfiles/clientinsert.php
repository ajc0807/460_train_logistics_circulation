<!--
This file is used to edit the records in table persons. You do not need to run this by yourself. 
This is called by the editrecord.php.
-->
<?php
//if (!empty($_GET['CID'])){
//$CID = $_GET['CID']; //the value of pid is received from the editrecord.php page
//}

$servername = "localhost";
$username = "root";
$password = "";
$dbname  = "tlc";

// Create connection to database
$conn = new mysqli($servername, $username, $password, $dbname);

//Things to do, after the "submitbtn" button is clicked.
if(isset($_POST['submitbtn']))
{
        $insert_bind=$conn->prepare("INSERT INTO client (CID, Name, BirthYear) VALUES (DEFAULT, ?, ?)");
        $insert_bind->bind_param("si", $_POST['Nametb'], $_POST['BirthYeartb']);
        $insert_bind->execute();
        $insert_bind->close();

        if($insert_bind) //if the update is done successfully
		{
		echo "Records inserted successfully";
		}
            echo '<form>
                        <a href="clientall.php"> Return to table</a>
                </form>';
}
?>

<form action="" method="post">
<?php
 echo "<table style='border: solid 1px black;'>
	<tr>
	    <th>CID</th>
	    <th>Name </th>
	    <th>BirthYear</th>
	</tr>";

	echo '<tr>
		<!-- just for simplicity, we assume the PK value cannot be updated, as such, it is "readonly" -->
		<td><input type="text" name="CIDtb" " readonly/></td>
		<td><input type="text" name="Nametb" "/></td>
		<td><input type="text" name="BirthYeartb" "/></td>
	      <tr>';
 echo "</table>";
?>
<input type="submit" value="Submit" name="submitbtn"/>
</form>

