<!--
This file is used to edit the records in table persons. You do not need to run this by yourself. 
This is called by the editrecord.php.
-->
<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname  = "tlc";

// Create connection to database
$conn = new mysqli($servername, $username, $password, $dbname);


if(isset($_GET['mode']) == 'update'){
    if (!empty($_GET['SID'])){
        $sid = $_GET['SID'];// get the id value from url parameters
    }
}

//Things to do, after the "updatebtn" button is clicked.
if(isset($_POST['updatebtn']))
{
	$update_bind=$conn->prepare("UPDATE shipment SET PackageWeightLbs= ?, ArrivalCondition= ?, RailcarNumber= ?, Cost= ?, RailcarType= ?, OrderedBy= ?, AssignedBy= ? WHERE SID= ?");
        $update_bind->bind_param("isiisiii", $_POST['PackageWeightLbstb'], $_POST['ArrivalConditiontb'], $_POST['RailcarNumbertb'], $_POST['Costtb'], $_POST['RailcarTypetb'], $_POST['OrderedBytb'], $_POST['AssignedBytb'], $sid);
        $update_bind->execute();
        $update_bind->close();

        if($update_bind) //if the update is done successfully
		{
		echo "Records updated successfully";
		}
            echo '<form>
                <a href="shipmentall.php"> Return to Table</a>
                </form>';

}

//when the page is loaded (also after the update is effective), the information of the selected (updated) record is loaded
$sql = "SELECT * FROM shipment WHERE SID='$sid'";
$result = $conn->query($sql);
?>

<form action="" method="post">
<?php
if($result->num_rows > 0){//if the record is found (which is expected!), then display it in a table
 echo "<table style='border: solid 1px black;'>
	<tr>
	    <th>SID</th>
	    <th>Package WeightLbs</th>
	    <th>Arrival Condition</th>
	    <th>Railcar Number</th>
	    <th>Cost</th>
	    <th>Railcar Type</th>
	    <th>Ordered By</th>
	    <th>Assigned By</th>
	</tr>";
}

while ($row = $result -> fetch_assoc()){//fetch the attributes to put in the designated textboxes
	echo '<tr>
		<!-- just for simplicity, we assume the PK value cannot be updated, as such, it is "readonly" -->
		<td><input type="text" name="SIDtb" value="'.$row['SID'].'" readonly/></td>
		<td><input type="text" name="PackageWeightLbstb" value="'.$row['PackageWeightLbs'].'"/></td>
		<td><input type="text" name="ArrivalConditiontb" value="'.$row['ArrivalCondition'].'"/></td>
		<td><input type="text" name="RailcarNumbertb" value="'.$row['RailcarNumber'].'"/></td>
		<td><input type="text" name="Costtb" value="'.$row['Cost'].'"/></td>
		<td><input type="text" name="RailcarTypetb" value="'.$row['RailcarType'].'"/></td>
		<td><input type="text" name="OrderedBytb" value="'.$row['OrderedBy'].'"/></td>
		<td><input type="text" name="AssignedBytb" value="'.$row['AssignedBy'].'"/></td>
	      <tr>';
}
 echo "</table>";
?>
<input type="submit" value="Update" name="updatebtn"/>

</form>

