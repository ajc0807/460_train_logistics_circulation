<!--
This file is used to edit the records in table persons. You do not need to run this by yourself. 
This is called by the editrecord.php.
-->
<?php
//if (!empty($_GET['BID'])){
//$BID = $_GET['BID']; //the value of pid is received from the editrecord.php page
//}

$servername = "localhost";
$username = "root";
$password = "";
$dbname  = "tlc";

// Create connection to database
$conn = new mysqli($servername, $username, $password, $dbname);

//Things to do, after the "submitbtn" button is clicked.
if(isset($_POST['submitbtn']))
{
        $insert_bind=$conn->prepare("INSERT INTO building (BID, Address, BuildingType, OnRailway, VisitsInTwoYears) VALUES (DEFAULT, ?, ?, ?, ?)");
        $insert_bind->bind_param("ssii", $_POST['Addresstb'], $_POST['BuildingTypetb'], $_POST['OnRailwaytb'], $_POST['VisitsInTwoYearstb']);
        $insert_bind->execute();
        $insert_bind->close();

	if($insert_bind) //if the update is done successfully
		{
		echo "Records inserted successfully";
		}
            echo '<form>
                        <a href="buildingall.php"> Return to table</a>
                </form>';
}
?>

<form action="" method="post">
<?php
 echo "<table style='border: solid 1px black;'>
	<tr>
	    <th>BID</th>
	    <th>Address </th>
	    <th>BuildingType </th>
	    <th>OnRailway </th>
	    <th>VisitsInTwoYears </th>
	</tr>";

	echo '<tr>
		<!-- just for simplicity, we assume the PK value cannot be updated, as such, it is "readonly" -->
		<td><input type="text" name="BIDtb"  readonly/></td>
		<td><input type="text" name="Addresstb" /></td>
		<td><input type="text" name="BuildingTypetb" /></td>
		<td><input type="text" name="OnRailwaytb" /></td>
		<td><input type="text" name="VisitsInTwoYearstb" /></td>
	      <tr>';
 echo "</table>";
?>
<input type="submit" value="Submit" name="submitbtn"/>
</form>

