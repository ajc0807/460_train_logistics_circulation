<!--
This file is used to edit the records in table persons. You do not need to run this by yourself. 
This is called by the editrecord.php.
-->
<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname  = "tlc";

// Create connection to database
$conn = new mysqli($servername, $username, $password, $dbname);


if(isset($_GET['mode']) == 'update'){
    if (!empty($_GET['RCID'])){
        $rcid = $_GET['RCID'];// get the id value from url parameters
    }
}

//Things to do, after the "updatebtn" button is clicked.
if(isset($_POST['updatebtn']))
{
	$update_bind=$conn->prepare("UPDATE railcar SET RailcarLocation= ?, RailcarType= ?, AttatchedTo= ?, TimeInUseHours = ? WHERE RCID= ?");
        $update_bind->bind_param("isiii", $_POST['RailcarLocationtb'], $_POST['RailcarTypetb'], $_POST['AttatchedTotb'], $_POST['TimeInUseHourstb'], $rcid);
        $update_bind->execute();
        $update_bind->close();

        if($update_bind) //if the update is done successfully
		{
		echo "Records updated successfully";
		}
            echo '<form>
                <a href="railcarall.php"> Return to Table</a>
                </form>';

}

//when the page is loaded (also after the update is effective), the information of the selected (updated) record is loaded
$sql = "SELECT * FROM railcar WHERE RCID='$rcid'";
$result = $conn->query($sql);
?>

<form action="" method="post">
<?php
if($result->num_rows > 0){//if the record is found (which is expected!), then display it in a table
 echo "<table style='border: solid 1px black;'>
	<tr>
	    <th>RCID</th>
	    <th>RailcarLocation</th>
	    <th>RailcarType</th>
	    <th>AttatchedTo</th>
	    <th>TimeInUseHours</th>
	</tr>";
}

while ($row = $result -> fetch_assoc()){//fetch the attributes to put in the designated textboxes
	echo '<tr>
		<!-- just for simplicity, we assume the PK value cannot be updated, as such, it is "readonly" -->
		<td><input type="text" name="RCIDtb" value="'.$row['RCID'].'" readonly/></td>
		<td><input type="text" name="RailcarLocationtb" value="'.$row['RailcarLocation'].'"/></td>
		<td><input type="text" name="RailcarTypetb" value="'.$row['RailcarType'].'"/></td>
		<td><input type="text" name="AttatchedTotb" value="'.$row['AttatchedTo'].'"/></td>
		<td><input type="text" name="TimeInUseHourstb" value="'.$row['TimeInUseHours'].'"/></td>
	      <tr>';
}
 echo "</table>";
?>
<input type="submit" value="Update" name="updatebtn"/>

</form>

