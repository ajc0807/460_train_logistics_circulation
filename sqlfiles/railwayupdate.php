<!--
This file is used to edit the records in table persons. You do not need to run this by yourself. 
This is called by the editrecord.php.
-->
<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname  = "tlc";

// Create connection to database
$conn = new mysqli($servername, $username, $password, $dbname);


if(isset($_GET['mode']) == 'update'){
    if (!empty($_GET['RWID'])){
        $rwid = $_GET['RWID'];// get the id value from url parameters
    }
}

//Things to do, after the "updatebtn" button is clicked.
if(isset($_POST['updatebtn']))
{
	$update_bind=$conn->prepare("UPDATE railway SET Capacity= ? WHERE RWID= ?");
        $update_bind->bind_param("ii", $_POST['Capacitytb'], $rwid);
        $update_bind->execute();
        $update_bind->close();

        if($update_bind) //if the update is done successfully
		{
		echo "Records updated successfully";
		}
            echo '<form>
                <a href="railwayall.php"> Return to Table</a>
                </form>';

}

//when the page is loaded (also after the update is effective), the information of the selected (updated) record is loaded
$sql = "SELECT * FROM railway WHERE RWID='$rwid'";
$result = $conn->query($sql);
?>

<form action="" method="post">
<?php
if($result->num_rows > 0){//if the record is found (which is expected!), then display it in a table
 echo "<table style='border: solid 1px black;'>
	<tr>
	    <th>RWID</th>
	    <th>Capacity</th>
	</tr>";
}

while ($row = $result -> fetch_assoc()){//fetch the attributes to put in the designated textboxes
	echo '<tr>
		<!-- just for simplicity, we assume the PK value cannot be updated, as such, it is "readonly" -->
		<td><input type="text" name="RWIDtb" value="'.$row['RWID'].'" readonly/></td>
		<td><input type="text" name="Capacitytb" value="'.$row['Capacity'].'"/></td>
	      <tr>';
}
 echo "</table>";
?>
<input type="submit" value="Update" name="updatebtn"/>

</form>

