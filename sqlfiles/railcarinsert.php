<!--
This file is used to edit the records in table persons. You do not need to run this by yourself. 
This is called by the editrecord.php.
-->
<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname  = "tlc";

// Create connection to database
$conn = new mysqli($servername, $username, $password, $dbname);

//Things to do, after the "submitbtn" button is clicked.
if(isset($_POST['submitbtn']))
{
        $insert_bind=$conn->prepare("INSERT INTO railcar (RCID, RailcarLocation, RailcarType, AttatchedTo, TimeInUseHours) VALUES (DEFAULT, ?, ?, ?, ?)");
        $insert_bind->bind_param("isii", $_POST['RailcarLocationtb'], $_POST['RailcarTypetb'], $_POST['AttatchedTotb'], $_POST['TimeInUseHourstb']);
        $insert_bind->execute();
        $insert_bind->close();

        if($insert_bind) //if the update is done successfully
		{
		echo "Records inserted successfully";
		}
            echo '<form>
                        <a href="railcarall.php"> Return to table</a>
                </form>';
}
?>

<form action="" method="post">
<?php
 echo "<table style='border: solid 1px black;'>
	<tr>
	    <th>RCID</th>
	    <th>RailcarLocation </th>
	    <th>RailcarType</th>
	    <th>AttatchedTo</th>
	    <th>TimeInUseHours</th>
	</tr>";

	echo '<tr>
		<!-- just for simplicity, we assume the PK value cannot be updated, as such, it is "readonly" -->
		<td><input type="text" name="RCIDtb"  readonly/></td>
		<td><input type="text" name="RailcarLocationtb" /></td>
		<td><input type="text" name="RailcarTypetb" /></td>
		<td><input type="text" name="AttatchedTotb" /></td>
		<td><input type="text" name="TimeInUseHourstb" /></td>
	      <tr>';
 echo "</table>";
?>
<input type="submit" value="Submit" name="submitbtn"/>
</form>

