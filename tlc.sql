-- Disable Key Checks
SET FOREIGN_KEY_CHECKS=0;
--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE `client` (
  `CID` int NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) DEFAULT NULL,
  `BirthYear` int(4) DEFAULT NULL,
  PRIMARY KEY (`CID`)
); 

--
-- Inserting data for table `client`
--

INSERT INTO `client` VALUES (DEFAULT, 'Cool Carl', 1945),(DEFAULT, 'Carlton Banks', 1985),(DEFAULT, 'Will Smith', 1984);

--
-- Table structure for table `railway`
--

DROP TABLE IF EXISTS `railway`;
CREATE TABLE `railway` (
  `RWID` int NOT NULL AUTO_INCREMENT,
  `Capacity` int NOT NULL,
  PRIMARY KEY (`RWID`)
);

--
-- Inserting data for table `railway`
--

INSERT INTO `railway` VALUES (1, 4000000),(2, 200000000);

--
-- Table structure for table `building`
--

DROP TABLE IF EXISTS `building`;
CREATE TABLE `building` (
  `BID` int NOT NULL AUTO_INCREMENT,
  `Address` varchar(20) DEFAULT NULL,
  `BuildingType` varchar(45) DEFAULT NULL,
  `OnRailway` int DEFAULT NULL,
  `VisitsInTwoYears` int (20) DEFAULT NULL,
  CONSTRAINT `OnRailway` FOREIGN KEY (`OnRailway`) REFERENCES `railway` (`RWID`) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (`BID`)
);

--
-- Inserting data for table `building`
--

INSERT INTO `building` VALUES (DEFAULT, '123 Test Street','station', 1, 2922),(DEFAULT, '124 Test Street','station', 1, 12922),(DEFAULT, '123 Mulberry Lane','shuntyard',1, 38208),(DEFAULT, '123 Street Circle','service bay',2, 12937);

--
-- Table structure for table `train`
--

DROP TABLE IF EXISTS `train`;
CREATE TABLE `train` (
  `TID` int NOT NULL AUTO_INCREMENT,
  `TrainLocation` int DEFAULT NULL,
  `Tcondition` varchar(20) DEFAULT NULL,
  `TimeInUseHours` int (20) DEFAULT NULL,
  `NextMaintenanceCheck` int (20) DEFAULT NULL,
  `WeightCapacityLbs` int(40),
  CONSTRAINT `TrainLocation` FOREIGN KEY (`TrainLocation`) REFERENCES `building` (`BID`) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (`TID`)
);

--
-- Inserting data for table `train`
--

INSERT INTO `train` VALUES (DEFAULT, 0, 'Good', 34, 500, 221421823),(DEFAULT, 2, 'Good', 334, 500, 300400801),(DEFAULT, 0, 'Poor', 1334723434, 1500, 300400800),(DEFAULT, 0, 'Poor', 1339999994, 1500, 300400800);

--
-- Table structure for table `railcar`
--

DROP TABLE IF EXISTS `railcar`;
CREATE TABLE `railcar` (
  `RCID` int NOT NULL AUTO_INCREMENT,
  `RailcarLocation` int DEFAULT NULL,
  `RailcarType` varchar(45) DEFAULT NULL,
  `TimeInUseHours` int (20) DEFAULT NULL,
  `AttatchedTo` int DEFAULT NULL,
  CONSTRAINT `AttatchedTo` FOREIGN KEY (`AttatchedTo`) REFERENCES `train` (`TID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `RailcarLocation` FOREIGN KEY (`RailcarLocation`) REFERENCES `railway` (`RWID`) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (`RCID`)
);

--
-- Inserting data for table `railcar`
--

INSERT INTO `railcar` VALUES (DEFAULT, 1,'flatbed', 100270329, DEFAULT),(DEFAULT, 1,'flatbed', 150, DEFAULT),(DEFAULT, 1,'flatbed', 200, 1),(DEFAULT, 2,'tank', 1, DEFAULT),(DEFAULT, 2,'tank', 1888, 3),(DEFAULT, 2,'tank', 100000001, 4),(DEFAULT, 2,'box', 212398191, 4),(DEFAULT, 2,'box', 12991, 2),(DEFAULT, 2,'box', 28191, 1);

--
-- Table structure for table `engineer`
--

DROP TABLE IF EXISTS `engineer`;
CREATE TABLE `engineer` (
  `EID` int NOT NULL AUTO_INCREMENT,
  `Name` varchar(25) DEFAULT NULL, 
  `Salary` int(25) NOT NULL, 
  `CurrentTrain` int DEFAULT NULL, 
  `NextTrain` int DEFAULT NULL, 
  CONSTRAINT `CurrentTrain` FOREIGN KEY (`CurrentTrain`) REFERENCES `train` (`TID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `NextTrain` FOREIGN KEY (`NextTrain`) REFERENCES `train` (`TID`) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (`EID`)
);

--
-- Inserting data for table `engineer`
--

INSERT INTO `engineer` VALUES (DEFAULT, 'Bob',80000, DEFAULT, DEFAULT),(DEFAULT, 'Alex',80001, 2, 1),(DEFAULT, 'Alexa',90001, 1, 2),(DEFAULT, 'Alex',80011, DEFAULT, DEFAULT),(DEFAULT, 'Jo',80101, 4, 3),(DEFAULT, 'Jim',190000, 3, 4);

----
---- Table structure for table `station`
----
--
--DROP TABLE IF EXISTS `station`;
--CREATE TABLE `station` (
--  `BID` int NOT NULL AUTO_INCREMENT,
--  `Address` varchar(20) DEFAULT NULL,
--  `BuildingType` varchar(45) DEFAULT NULL,
--  `OnRailway` int DEFAULT NULL,
--  CONSTRAINT `OnRailway` FOREIGN KEY (`OnRailway`) REFERENCES `railway` (`RWID`) ON DELETE CASCADE ON UPDATE CASCADE, 
--  PRIMARY KEY (`BID`)
--);
--
----
---- Inserting data for table `station`
----
--
--INSERT INTO `station` VALUES (1, '123 Test Street','station', 1),(2, '123 Mulberry Lane','shuntyard',1),(3, '123 Street Circle','service bay',2);
--
----
---- Table structure for table `shuntYard`
----
--
--DROP TABLE IF EXISTS `shuntYard`;
--CREATE TABLE `shuntYard` (
--  `BID` int NOT NULL AUTO_INCREMENT,
--  `Address` varchar(20) DEFAULT NULL,
--  `BuildingType` varchar(45) DEFAULT NULL,
--  `OnRailway` int DEFAULT NULL,
--  CONSTRAINT `OnRailway` FOREIGN KEY (`OnRailway`) REFERENCES `railway` (`RWID`) ON DELETE CASCADE ON UPDATE CASCADE,
--  PRIMARY KEY (`BID`)
--);
--
----
---- Inserting data for table `shuntYard`
----
--
--INSERT INTO `shuntYard` VALUES (1, '123 Test Street','station', 1),(2, '123 Mulberry Lane','shuntyard',1),(3, '123 Street Circle','service bay',2);
--
----
---- Table structure for table `serviceBay`
----
--
--DROP TABLE IF EXISTS `serviceBay`;
--CREATE TABLE `serviceBay` (
--  `BID` int NOT NULL AUTO_INCREMENT,
--  `Address` varchar(20) DEFAULT NULL,
--  `BuildingType` varchar(45) DEFAULT NULL,
--  `OnRailway` int DEFAULT NULL,
--  CONSTRAINT `OnRailway` FOREIGN KEY (`OnRailway`) REFERENCES `railway` (`RWID`) ON DELETE CASCADE ON UPDATE CASCADE,
--  PRIMARY KEY (`BID`)
--);
--
----
---- Inserting data for table `serviceBay`
----
--
--INSERT INTO `serviceBay` VALUES (1, '123 Test Street','station', 1),(2, '123 Mulberry Lane','shuntyard',1),(3, '123 Street Circle','service bay',2);

--
-- Table structure for table `trackAvailable`
--

DROP TABLE IF EXISTS `trackAvailable`;
CREATE TABLE `trackAvailable` (
  `RWID` int NOT NULL,
  `Hour` int NOT NULL,
  `Available` int NOT NULL,
  PRIMARY KEY (`RWID`, `Hour`),
  CONSTRAINT `RWID` FOREIGN KEY (`RWID`) REFERENCES `railway` (`RWID`) ON DELETE CASCADE ON UPDATE CASCADE
);

--
-- Inserting data for table `trackAvailable`
--

INSERT INTO `trackAvailable` VALUES (1,1, 1), (1,2,1),(1,3,1),(1,4,1),(1,5,1),(1,6,1),(1,7,1),(1,8,1),(1,9,1),(1,10,0),(1,11,0),(1,12,0),(1,13,0),(1,14,0),(1,15,0),(1,16,0),(1,17,0),(1,18,1),(1,19,1),(1,20,1),(1,21,1),(1,22,1),(1,23,1),(1,24,1),(2,1,1),(2,2,1),(2,3,1),(2,4,0),(2,5,0),(2,6,0),(2,7,0),(2,8,0),(2,9,0),(2,10,0),(2,11,1),(2,12,1),(2,13,1),(2,14,1),(2,15,1),(2,16,1),(2,17,1),(2,18,1),(2,19,1),(2,20,1),(2,21,1),(2,22,1),(2,23,1),(2,24,1);
--INSERT INTO `trackAvailable` VALUES (1,1, 1), (1,2,1),(1,3,1),(1,4,1),(1,5,1),(1,6,1),(1,7,1),(1,8,1),(1,9,1),(1,18,1),(1,19,1),(1,20,1),(1,21,1),(1,22,1),(1,23,1),(1,24,1),(2,1,1),(2,2,1),(2,3,1),(2,10,1),(2,11,1),(2,12,1),(2,13,1),(2,14,1),(2,15,1),(2,16,1),(2,17,1),(2,20,1),(2,21,1),(2,22,1),(2,23,1),(2,24,1);

--
-- Table structure for table `shipment`
--

DROP TABLE IF EXISTS `shipment`;
CREATE TABLE `shipment` (
  `SID` int NOT NULL AUTO_INCREMENT,
  `PackageWeightLbs` int(40) NOT NULL,
  `ArrivalCondition` varchar(25) DEFAULT NULL,
  `RailcarNumber` int(5) NOT NULL,
  `Cost` int(5) NOT NULL DEFAULT 0,
  `RailcarType` varchar(45) NOT NULL,
  `OrderedBy` int(25) NOT NULL,
  CONSTRAINT `OrderedBy` FOREIGN KEY (`OrderedBy`) REFERENCES `client` (`CID`) ON DELETE CASCADE ON UPDATE CASCADE,
  `AssignedBy` int(25) DEFAULT NULL,
  CONSTRAINT `AssignedBy` FOREIGN KEY (`AssignedBy`) REFERENCES `dispatcher` (`DID`) ON DELETE CASCADE ON UPDATE CASCADE,
   PRIMARY KEY (`SID`)
);

--
-- Inserting data for table `shipment`
--

INSERT INTO `shipment` VALUES (DEFAULT, 20000, 'Good', 2, 10000, 'flatbed', 1, 1),(DEFAULT, 20000, '', 2, 10000, 'tank', 1, 2),(DEFAULT, 300000, '', 3, 10000, 'box', 2, 1);

--
-- Table structure for table `dispatcher`
--

DROP TABLE IF EXISTS `dispatcher`;
CREATE TABLE `dispatcher` (
  `DID` int AUTO_INCREMENT ,
  `Dname` varchar(40) NOT NULL,
  `Salary` int NOT NULL,
  `WorksAt` int NOT NULL,
  CONSTRAINT `WorksAt` FOREIGN KEY (`WorksAt`) REFERENCES `building` (`BID`) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (`DID`)
);

--
-- Inserting data for table `dispatcher`
--

INSERT INTO `dispatcher` VALUES (DEFAULT, 'Paul', 80000, 1),(DEFAULT, 'Big Paul', 80001, 2),(DEFAULT, 'John Smith', 70001, 1);
-- Enable Key Checks
SET FOREIGN_KEY_CHECKS=1; 
